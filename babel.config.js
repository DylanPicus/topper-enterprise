module.exports = {
  presets: [
    "@babel/preset-env",
    "@babel/preset-react", // This line is crucial for transforming JSX
  ],
};
