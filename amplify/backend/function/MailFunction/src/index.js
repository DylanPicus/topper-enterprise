/* Amplify Params - DO NOT EDIT
	ENV
	REGION
Amplify Params - DO NOT EDIT */
var aws = require("aws-sdk");
var ses = new aws.SES({ region: "eu-central-1" });

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
exports.handler = async (event) => {
  const headers = {
    "Access-Control-Allow-Origin": "http://localhost:3000",
  };
  let statusCode;
  let body;

  const params = {
    Destination: {
      ToAddresses: ["dylanpicus99@gmail.com"],
    },
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data: `<h1>Action!</h1><p>Delete data from user ${event["pathParameters"].proxy}</p>`,
        },
        Text: {
          Charset: "UTF-8",
          Data: "Hello! This is a test email",
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: "Delete info pls",
      },
    },
    Source: "dylanpicus99@gmail.com",
  };

  try {
    const data = await ses.sendEmail(params).promise();
    console.log("Email sent:", data);
    body = "Mail sent";
    statusCode = 200;
  } catch (error) {
    console.log("Error sending email", error);
    statusCode = 400;
    body = "Failed";
  }
  return {
    statusCode,
    body,
    headers,
  };
};
