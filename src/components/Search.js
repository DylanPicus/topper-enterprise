import React from "react";
import { useEffect, useState } from "react";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
import {
  TextField,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  ListItemButton,
  Button,
} from "@mui/material";

import { Auth, API } from "aws-amplify";
import { createfollow } from "../api/follow/create_follow";

const Search = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [users, setUsers] = useState([]);
  const [relations, setRelations] = useState([]);
  const [authenticatedUserId, setAuthenticatedUserId] = useState(null);

  useEffect(() => {
    async function getAuthUser() {
      const user = await Auth.currentAuthenticatedUser();
      getFollowed(user.attributes.sub);
      setAuthenticatedUserId(user.attributes.sub);
    }
    getAuthUser();
  }, []);

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSearchSubmit = async (event) => {
    event.preventDefault();
    const query = /* GraphQL */ `
    query SearchUsers {
      searchUsers(limit: 5, filter: { name: { wildcard: "*${searchTerm}*" } }) {
        items {
          id
          name
          username
          profileImage
        }
      }
    }
  `;

    try {
      const apiData = await API.graphql({
        query: query,
      });

      console.log(apiData);
      let results = apiData.data.searchUsers.items;
      let filteredResult = results.filter(
        (user) => user.id !== authenticatedUserId
      );

      const completeUserList = filteredResult.map((user) => ({
        ...user,
        isAdded: false,
      }));

      const filteredFriendsList = completeUserList.map((obj) => {
        if (relations.some((item) => item.receiverId === obj.id)) {
          return { ...obj, isAdded: true };
        }
        return obj;
      });

      setUsers(filteredFriendsList);
    } catch (error) {
      console.error("Error searching users", error);
    }
  };

  async function followUser(targetUserId) {
    const updatedUserList = users.map((user) =>
      targetUserId === user.id ? { ...user, isAdded: true } : user
    );
    console.log(updatedUserList);
    setUsers(updatedUserList);

    const sourceUser = await Auth.currentAuthenticatedUser();
    const sourceUserId = sourceUser.attributes.sub;

    const data = {
      requesterId: sourceUserId,
      receiverId: targetUserId,
    };

    await createfollow("/follow", data);
  }

  async function getFollowed(userId) {
    const query = /* GraphQL */ `
    query SearchRelations {
      searchRelations(limit: 50, filter: { requesterId: { eq: "${userId}" } }) {
        items {
          id
          requesterId
          receiverId
        }
      }
    }
  `;

    try {
      const apiData = await API.graphql({
        query: query,
      });

      console.log(apiData);
      let results = apiData.data.searchRelations.items;

      setRelations(results);
    } catch (error) {
      console.error("Error fetching followed relations", error);
    }
  }

  return (
    <div>
      <h2>Search</h2>
      <form onSubmit={handleSearchSubmit}>
        <TextField
          fullWidth
          variant="outlined"
          placeholder="Search for users..."
          size="small"
          onChange={handleSearchChange}
        />
      </form>
      {users.length === 0 ? (
        <h3>No user found</h3>
      ) : (
        users.map((user) => (
          <List key={user.id}>
            <ListItem key={user.id}>
              <ListItemAvatar>
                <Avatar src={user.name} />
              </ListItemAvatar>
              <ListItemText primary={user.name} />
              <ListItemButton>
                <Button
                  data-testid={`add-button-${user.id}`}
                  disabled={user.isAdded}
                  onClick={() => followUser(user.id)}
                >
                  <PersonAddIcon />
                  {user.isAdded ? "Added" : "Add"}
                </Button>
              </ListItemButton>
            </ListItem>
          </List>
        ))
      )}
    </div>
  );
};

export default Search;
