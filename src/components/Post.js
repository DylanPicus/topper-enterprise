import React from "react";
import { useEffect, useState } from "react";

import {
  Avatar,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  IconButton,
  Typography,
  Button,
} from "@mui/material";

import { Image } from "@aws-amplify/ui-react";

import FavoriteIcon from "@mui/icons-material/Favorite";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";

//import image1 from "../assets/climb1.png";

import { Storage } from "aws-amplify";

const Post = (props) => {
  const [image, setImage] = useState(null);
  const [blur, setBlur] = useState(false);
  const post = props.postData;

  useEffect(() => {
    async function fetchImage() {
      const image = await Storage.get(post.image);
      setImage(image);
    }
    fetchImage();
  }, []);

  useEffect(() => {
    if (post.contentStatus === "Rejected") {
      setBlur(true);
    }
  }, [post.contentStatus]);

  return (
    <Card>
      <CardHeader avatar={<Avatar src={image} />} title={post.id} />
      {image && (
        <div style={{ position: "relative" }}>
          <Image
            src={image}
            style={{
              width: "100%",
              objectFit: "cover",
              filter: blur ? "blur(8px)" : "none",
            }}
          />

          {blur && (
            <div
              style={{
                position: "absolute",
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "rgba(0,0,0,0.5)",
              }}
            >
              <Button
                variant="contained"
                style={{ backgroundColor: "rgba(255, 255, 255, 0.5)" }}
                onClick={() => setBlur(false)}
              >
                Click to show explicit content.
              </Button>
            </div>
          )}
        </div>
      )}

      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {post.title}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {post.grade}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {post.location}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton disabled={true}>
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="comment">
          <ChatBubbleOutlineIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default Post;
