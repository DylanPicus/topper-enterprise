import React, { useState, useEffect } from "react";
import {
  Box,
  Typography,
  Avatar,
  Grid,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
} from "@mui/material";

import { Auth, API } from "aws-amplify";
import * as XLSX from "xlsx";

import Post from "../components/Post";
import { getuser } from "../api/onboarding/get_user";
import { deleteinfo } from "../api/mail/delete_data";

const Profile = () => {
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("John Doe");
  const [followers, setFollowers] = useState(120);
  const [following, setFollowing] = useState(350);
  const [discipline, setDiscipline] = useState("Computer Science");
  const [highestGrade, setHighestGrade] = useState("PhD");
  const [bio, setBio] = useState("Software Developer");
  const [profilePicture, setProfilePicture] = useState(null);
  const [posts, setPosts] = useState([]);

  const [user, setUser] = useState({});
  const [userId, setUserId] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    async function fetchUser() {
      const user = await Auth.currentAuthenticatedUser();
      setUserId(user.attributes.sub);
      const resp = await getuser("/user/" + user.attributes.sub);
      console.log(resp.data.getUser);
      setUser(resp.data.getUser);
    }

    async function fetchPosts() {
      const user = await Auth.currentAuthenticatedUser();
      const userId = user.attributes.sub;
      const query = /* GraphQL */ `
      query SearchPosts {
        searchPosts(limit: 5, filter: { userId: { eq: "${userId}" } }) {
          items {
            id
            title
            image
            grade
            location
            userId
          }
        }
      }
    `;

      const apiData = await API.graphql({ query: query });
      const postsFromAPI = apiData.data.searchPosts.items;
      console.log(postsFromAPI);
      setPosts(postsFromAPI);
      setIsLoading(false);
    }
    fetchUser();
    fetchPosts();
  }, []);

  const handleEditInfo = () => {
    setOpen(true);
  };

  const handleDataRequest = () => {
    let wb = XLSX.utils.book_new();
    let users = [];
    users.push(user);
    let usersheet = XLSX.utils.json_to_sheet(users);
    let postsheet = XLSX.utils.json_to_sheet(posts);
    // let likesheet = XLSX.utils.json_to_sheet(user);
    // let commentsheet = XLSX.utils.json_to_sheet(user);

    /* Add worksheet to workbook */
    XLSX.utils.book_append_sheet(wb, usersheet, "User Data");
    XLSX.utils.book_append_sheet(wb, postsheet, "Post Data");
    // XLSX.utils.book_append_sheet(wb, likesheet, "User Data");
    // XLSX.utils.book_append_sheet(wb, commentsheet, "User Data");

    /* Write workbook and download */
    XLSX.writeFile(wb, "DataExport.xlsx");
  };

  const handleAccountDeletion = async () => {
    const resp = await deleteinfo("/delete/" + userId);
    console.log(resp);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = () => {
    setOpen(false);
  };

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return (
    <Box>
      <Box display="flex" flexDirection="column" alignItems="center" mb={2}>
        <Avatar
          alt={name}
          src={profilePicture}
          sx={{ width: 100, height: 100, marginBottom: 1 }}
        />
        <Typography variant="h5" component="div">
          {user.name}
        </Typography>
        <Typography variant="subtitle1" color="text.secondary">
          {followers} followers - {following} following
        </Typography>
        <Button
          variant="outlined"
          color="primary"
          onClick={handleEditInfo}
          sx={{ mt: 1 }}
        >
          Edit Info
        </Button>
        <Button
          variant="outlined"
          color="primary"
          onClick={handleDataRequest}
          sx={{ mt: 1 }}
        >
          Request Data
        </Button>
        <Button
          variant="outlined"
          color="primary"
          onClick={handleAccountDeletion}
          sx={{ mt: 1 }}
        >
          Delete Profile
        </Button>
      </Box>
      <Box display="flex" justifyContent="space-evenly" mb={3}>
        <Typography variant="body1">
          <strong>Discipline:</strong> {user.discipline}
        </Typography>
        {/* <Typography variant="body1">
          <strong>Highest Grade:</strong> {highestGrade}
        </Typography> */}
        <Typography variant="body1">
          <strong>Bio:</strong> {user.bio}
        </Typography>
      </Box>
      <Box>
        <Typography variant="h6" component="div" mb={2}>
          Posts
        </Typography>
        <Grid container spacing={2}>
          {posts.map((post, index) => (
            <Grid item xs={4} key={index}>
              {<Post key={index} postData={post} />}
            </Grid>
          ))}
        </Grid>
      </Box>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Edit Info</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            fullWidth
          />
          <TextField
            margin="dense"
            label="Discipline"
            value={discipline}
            onChange={(e) => setDiscipline(e.target.value)}
            fullWidth
          />
          {/* <TextField
            margin="dense"
            label="Highest Grade"
            value={highestGrade}
            onChange={(e) => setHighestGrade(e.target.value)}
            fullWidth
          /> */}
          <TextField
            margin="dense"
            label="Bio"
            value={bio}
            onChange={(e) => setBio(e.target.value)}
            fullWidth
            multiline
            rows={4}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSave} color="primary" variant="contained">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

export default Profile;
