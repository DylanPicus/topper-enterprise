import React from "react";
import { useEffect, useState } from "react";
import { API, Auth } from "aws-amplify";

import Post from "../components/Post";
import { getrelations } from "../api/relation/get_relations";

function Feed() {
  const [posts, setPosts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    async function fetchPosts() {
      try {
        // know who my friends are
        const user = await Auth.currentAuthenticatedUser();
        const userId = user.attributes.sub;

        const data = await getrelations("/follow/" + userId);
        const friends = data.data.searchRelations.items;

        const friendIds = friends.map((item) => item.receiverId);

        for (const id of friendIds) {
          await getFriendPosts(id);
        }

        setIsLoading(false);
      } catch (error) {
        setError(error);
        setIsLoading(false);
      }
    }

    fetchPosts();
  }, []);

  async function getFriendPosts(id) {
    const query = /* GraphQL */ `
      query SearchPosts {
        searchPosts(limit: 15, filter: { userId: { eq: "${id}" } }) {
          items {
            id
            title
            image
            grade
            location
            userId
            contentStatus
          }
        }
      }
    `;

    try {
      const apiData = await API.graphql({
        query: query,
      });

      const postsFromAPI = apiData.data.searchPosts.items;
      setPosts(postsFromAPI);
    } catch (error) {
      console.error("Error fetching posts", error);
    }
  }

  if (isLoading) {
    return <p>Loading posts...</p>;
  }

  if (error) {
    return <p>Error loading posts</p>;
  }

  return (
    <div className="feed-container">
      {posts.map((post, index) => (
        <Post key={index} postData={post} />
      ))}
    </div>
  );
}

export default Feed;
