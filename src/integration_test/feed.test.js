import React from "react";
import { render, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import { API, Auth } from "aws-amplify";
import { getrelations } from "../api/relation/get_relations";
import Feed from "../pages/Feed";

jest.mock("@aws-amplify/auth");
jest.mock("@aws-amplify/storage");
jest.mock("../api/relation/get_relations");
jest.mock("@aws-amplify/api");
jest.mock("@aws-amplify/ui-react");

beforeAll(() => {
  global.URL.createObjectURL = jest.fn();
});

test("loads and displays posts", async () => {
  // Mock functions
  Auth.currentAuthenticatedUser.mockResolvedValue({
    attributes: { sub: "test-sub" },
  });

  getrelations.mockResolvedValue({
    data: {
      searchRelations: {
        items: [{ receiverId: "friend1" }, { receiverId: "friend2" }],
      },
    },
  });

  API.graphql.mockResolvedValue({
    data: {
      searchPosts: {
        items: [
          {
            id: "post1",
            title: "Post 1",
            grade: "Grade 1",
            location: "Location 1",
            userId: "friend1",
          },
          {
            id: "post2",
            title: "Post 2",
            grade: "Grade 2",
            location: "Location 2",
            userId: "friend2",
          },
        ],
      },
    },
  });

  // Render component
  const { getByText } = render(<Feed />);

  // Wait for posts to load
  await waitFor(() => {
    expect(getByText("Post 1")).toBeInTheDocument();
    expect(getByText("Post 2")).toBeInTheDocument();
  });
});

test("displays loading state", () => {
  // Render component
  const { getByText } = render(<Feed />);

  // Check that the loading message is displayed
  expect(getByText("Loading posts...")).toBeInTheDocument();
});

test("calls API with correct parameters", async () => {
  // Render component
  render(<Feed />);

  // Wait for API calls to complete
  await waitFor(() => {
    expect(getrelations).toHaveBeenCalledWith("/follow/test-sub");
    expect(API.graphql).toHaveBeenCalledWith({
      query: expect.stringContaining("friend1"),
    });
    expect(API.graphql).toHaveBeenCalledWith({
      query: expect.stringContaining("friend2"),
    });
  });
});

test("displays error message on API error", async () => {
  // Mock API call to throw an error
  getrelations.mockRejectedValue(new Error("API Error"));

  // Render component
  const { getByText } = render(<Feed />);

  // Wait for error message to display
  await waitFor(() => {
    expect(getByText("Error loading posts")).toBeInTheDocument();
  });
});
