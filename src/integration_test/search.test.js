import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Auth, API } from "aws-amplify";
import { createfollow } from "../api/follow/create_follow";
import Search from "../components/Search";

jest.mock("@aws-amplify/auth");
jest.mock("@aws-amplify/api");
jest.mock("../api/follow/create_follow");

beforeAll(() => {
  global.URL.createObjectURL = jest.fn();
});

test("loads and displays users on search", async () => {
  // Mock functions
  Auth.currentAuthenticatedUser.mockResolvedValue({
    attributes: { sub: "test-sub" },
  });

  API.graphql.mockImplementation((request) => {
    if (request.query.includes("SearchUsers")) {
      return Promise.resolve({
        data: {
          searchUsers: {
            items: [
              {
                id: "user1",
                name: "User 1",
                username: "user1",
                profileImage: null,
              },
              {
                id: "user2",
                name: "User 2",
                username: "user2",
                profileImage: null,
              },
            ],
          },
        },
      });
    }

    if (request.query.includes("SearchRelations")) {
      return Promise.resolve({
        data: {
          searchRelations: {
            items: [{ receiverId: "friend1" }, { receiverId: "friend2" }],
          },
        },
      });
    }
  });

  // Render component
  const { getByPlaceholderText, getByText } = render(<Search />);

  // Input search term
  fireEvent.change(getByPlaceholderText("Search for users..."), {
    target: { value: "n" },
  });

  // Submit search form
  fireEvent.submit(getByPlaceholderText("Search for users..."));

  // Wait for users to load
  await waitFor(() => {
    expect(getByText("User 1")).toBeInTheDocument();
    expect(getByText("User 2")).toBeInTheDocument();
  });
});

test("adds a user to follow", async () => {
  // Mock functions
  Auth.currentAuthenticatedUser.mockResolvedValue({
    attributes: { sub: "test-sub" },
  });

  createfollow.mockResolvedValue(true);

  API.graphql.mockImplementation((request) => {
    if (request.query.includes("SearchUsers")) {
      return Promise.resolve({
        data: {
          searchUsers: {
            items: [
              {
                id: "user1",
                name: "User 1",
                username: "user1",
                profileImage: null,
              },
              {
                id: "user2",
                name: "User 2",
                username: "user2",
                profileImage: null,
              },
            ],
          },
        },
      });
    }

    if (request.query.includes("SearchRelations")) {
      return Promise.resolve({
        data: {
          searchRelations: {
            items: [{ receiverId: "friend1" }, { receiverId: "friend2" }],
          },
        },
      });
    }
  });

  // Render component
  const { getByPlaceholderText, getByText, getByTestId } = render(<Search />);

  // Input search term
  fireEvent.change(getByPlaceholderText("Search for users..."), {
    target: { value: "user" },
  });

  // Submit search form
  fireEvent.submit(getByPlaceholderText("Search for users..."));

  // Wait for user to load
  await waitFor(() => {
    expect(getByText("User 1")).toBeInTheDocument();
  });

  // Click on "Add" button
  fireEvent.click(getByTestId("add-button-user1"));

  // Expect "Added" to be displayed
  await waitFor(() => {
    expect(getByText("Added")).toBeInTheDocument();
  });
});
